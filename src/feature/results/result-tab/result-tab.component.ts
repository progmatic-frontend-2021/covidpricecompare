import {Component, Input, OnInit} from '@angular/core';
import {TestType} from "../results.interface";

@Component({
  selector: 'app-result-tab',
  templateUrl: './result-tab.component.html',
  styleUrls: ['./result-tab.component.scss'],

})
export class ResultTabComponent implements OnInit {

  testTypes = TestType;
  @Input() location!:string;

  constructor() {
  }

  ngOnInit(): void {
  }

}
