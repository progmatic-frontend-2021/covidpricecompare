import {Component, OnInit} from '@angular/core';
import {ResultsService} from "../results.service";
import { ResultItem} from "../results.interface";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-result-container',
  templateUrl: './result-container.component.html',
  styleUrls: ['./result-container.component.scss']
})
export class ResultContainerComponent implements OnInit {

  resultItems: ResultItem[] = [];
  newLocation!: string;
  cities: string[] = [];

  constructor(private resultsService: ResultsService, private readonly router: Router, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
   this.newLocation = this.route.snapshot.queryParams['city'] || null;
  }

  selectedLocation(location: string) {
    this.newLocation = location;
    this.router.navigate(["results"], {queryParams: {city: this.newLocation}})
  }
}
