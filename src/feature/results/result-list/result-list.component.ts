import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ResultItem, TestType} from "../results.interface";
import {ResultsService} from "../results.service";
import { SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.scss']
})
export class ResultListComponent implements OnInit,OnChanges {

  @Input() testType!: TestType;
  @Input() location!: string | any;
  resultItems: ResultItem[] = [];

  constructor(private resultsService: ResultsService) { }

  getProductsByType(location: string, type: TestType) {
    this.resultsService.getItemsByLocation(type, location).subscribe(data => {
      this.resultItems = data;
    })
  }

  ngOnInit(): void {
    this.getProductsByType(this.location, this.testType)
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.location.currentValue != this.location.previousValue) {
    this.getProductsByType(changes.location.currentValue, this.testType)
    }
  }
}
