import {Provider} from "../providers/providers.interface";

export interface ResultItem {
  id: number
  providerId: number,
  provider: Provider,
  type: TestType,
  price: number
}

export enum TestType {
  PCR = "PCR",
  ANTIGEN = "Antigen",
  ANTIBODY = "Antitest"
}






