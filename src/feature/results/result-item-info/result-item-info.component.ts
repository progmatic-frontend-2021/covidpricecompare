import {Component, Input} from '@angular/core';
import {ResultItem} from "../results.interface";

@Component({
  selector: 'app-result-item-info',
  templateUrl: './result-item-info.component.html',
  styleUrls: ['./result-item-info.component.scss']
})
export class ResultItemInfoComponent{

  @Input() item!: ResultItem;

}
