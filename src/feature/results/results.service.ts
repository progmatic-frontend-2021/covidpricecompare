import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import { ResultItem, TestType} from "./results.interface";

@Injectable({
  providedIn: 'root'
})
export class ResultsService {

  static readonly API_PATH_PRODUCTS = "/products";

  constructor(private http: HttpClient) {
  }

  getItems(testType: TestType): Observable<ResultItem[]> {
    return this.http.get<ResultItem[]>(environment.API_URL + ResultsService.API_PATH_PRODUCTS, {
      params: new HttpParams().set("type", testType)
    })
  }

  getItemsByLocation(testType: TestType, location: string): Observable<any> {
    if (location === null) {
      return this.getItems(testType)
    } else {
      return this.http.get<ResultItem[]>(environment.API_URL + ResultsService.API_PATH_PRODUCTS, {
        params: new HttpParams().set("city", location).append("type", testType)
      })
    }
  }

}
