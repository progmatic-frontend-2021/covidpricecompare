import {Component, Input, OnInit} from '@angular/core';
import {ResultItem} from "../results.interface";
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-result-item',
  templateUrl: './result-item.component.html',
  styleUrls: ['./result-item.component.scss'],
  animations: [
    trigger('toggleContainer', [
      state('open', style({
        height: '200px'
      })),
      state('closed', style({
        height: '180px'
      })),
      transition('open => closed', [
        animate('.2s')
      ]),
      transition('closed => open', [
        animate('.3s')
      ])
    ]),
    trigger('toggleButton', [
      state('open', style({
        visibility: 'visible',
      })),
      state('closed', style({
        visibility: 'hidden'
      })),
      transition('open => closed', [
        animate('.2s')
      ]),
      transition('closed => open', [
        animate('.3s')
      ])
    ]),
    trigger('toggleBox', [
      state('open', style({
        visibility: 'visible',
        height: '70px',
      })),
      state('closed', style({
        visibility: 'hidden'
      })),
      transition('open => closed', [
        animate('.2s')
      ]),
      transition('closed => open', [
        animate('.3s')
      ]),
    ]),
    trigger('dropdownLocations', [
      state('open', style({
        visibility: 'visible',
        height: '100px',
        width: '900px'
      })),
      state('closed', style({
        visibility: 'hidden',
        height: '0px',
        width: '0px'
      })),
      transition('open => closed', [
        animate('.2s')
      ]),
      transition('closed => open', [
        animate('.3s')
      ])
    ]),
    trigger('dropdownContainer', [
      state('open', style({})),
      state('closed', style({})),
      transition('open => closed', [
        animate('.2s')
      ]),
      transition('closed => open', [
        animate('.3s')
      ])
    ]),
    trigger('dropdownArrow', [
      state('closed', style({ transform: 'rotate(0)' })),
      state('open', style({ transform: 'rotate(-180deg)' })),
      transition('open => closed', animate('400ms ease-out')),
      transition('closed => open', animate('400ms ease-in'))
    ])
  ]
})
export class ResultItemComponent implements OnInit {

  @Input() resultItem!: ResultItem;

  isOpen: boolean = false;
  isLocationsOpen: boolean = false;
  constructor() {
  }

  ngOnInit(): void {
    console.log("ResultItem", this.resultItem)
  }

  toggle() {
    this.isOpen = !this.isOpen;
    console.log(this.isOpen);
  }

  dropdown() {
    this.isLocationsOpen = !this.isLocationsOpen;
    console.log(this.isLocationsOpen);
  }

}


