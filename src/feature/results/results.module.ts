import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ResultContainerComponent} from './result-container/result-container.component';
import {ResultsRoutingModule} from "./results-routing.module";
import {ResultItemComponent} from './result-item/result-item.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from "@angular/material/icon";
import {ResultsService} from "./results.service";
import {MatTabsModule} from '@angular/material/tabs';
import {ResultTabComponent} from './result-tab/result-tab.component';
import {ResultListComponent} from './result-list/result-list.component';
import {RatingModule} from "../../shared/rating/rating.module";
import {LocationModule} from "../../shared/location-selector/location.module";
import {ResultItemInfoComponent} from './result-item-info/result-item-info.component';
import {RouterModule} from "@angular/router";
import {MatCardModule} from '@angular/material/card';
import {LocationsListComponent} from "../../shared/offer-locations-list/locations-list/locations-list.component";


@NgModule({
  declarations: [
    ResultContainerComponent,
    ResultItemComponent,
    ResultTabComponent,
    ResultListComponent,
    ResultItemInfoComponent,
    LocationsListComponent
  ],
  imports: [
    CommonModule,
    ResultsRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatTabsModule,
    RatingModule,
    LocationModule,
    RouterModule,
    MatCardModule
  ],
  providers: [
    ResultsService
  ]
})
export class ResultsModule {
}
