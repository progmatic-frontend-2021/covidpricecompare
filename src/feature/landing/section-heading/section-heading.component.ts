import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section-heading',
  templateUrl: './section-heading.component.html',
  styleUrls: ['./section-heading.component.scss']
})
export class SectionHeadingComponent implements OnInit {

  mainPicture_URL: string = "https://i.picsum.photos/id/201/500/300.jpg?hmac=v0GEqa-YATYYy9hkxWbMmoxVAp_JtNKUSpkfKBtwuBE";
  pcrImg_URL: string =  "https://i.picsum.photos/id/357/200/200.jpg?hmac=hHhE00vBpBPSjAiUhwzFKQi9PsCWu7sblLKC2rT6Fn8"
  antigenImg_URL: string = "https://i.picsum.photos/id/24/200/200.jpg?hmac=Tw5b43UPAehS5e4JyB0qMQysvfLBmu_GZ_iafWou3m8"
  antibodyImg_URL: string = "https://i.picsum.photos/id/936/200/200.jpg?hmac=IL7mo8CcTgJAO1CveVd2nT25CdqAuZ-htB7FYFW-4gQ"


  constructor() { }

  ngOnInit(): void {
  }

}
