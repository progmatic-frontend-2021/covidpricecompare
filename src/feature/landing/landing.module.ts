import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroComponent } from './hero/hero.component';
import { LandingComponent } from './landing.component';
import { SectionHeadingComponent } from './section-heading/section-heading.component';
import {OurNumbersModule} from "./our-numbers/our-numbers.module";
import {LocationModule} from "../../shared/location-selector/location.module";
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from "@angular/material/card";

@NgModule({
  declarations: [
    HeroComponent,
    LandingComponent,
    SectionHeadingComponent,
  ],
  imports: [
    CommonModule,
    OurNumbersModule,
    LocationModule,
    MatButtonModule,
    MatCardModule
  ],
  exports: [
    LandingComponent
  ]
})
export class LandingModule { }
