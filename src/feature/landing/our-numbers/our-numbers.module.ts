import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OurNumbersComponent} from "./our-numbers.component";
import {OurNumbersService} from "./our-numbers.service";
import {HttpClientModule} from "@angular/common/http";
import {MatCardModule} from "@angular/material/card";

@NgModule({
  declarations: [
    OurNumbersComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    MatCardModule
  ],
  exports: [
    OurNumbersComponent
  ],
  providers: [
    OurNumbersService
  ]
})
export class OurNumbersModule { }
