import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {ResultItem} from "../../results/results.interface";
import {Provider} from "../../providers/providers.interface";

@Injectable({
  providedIn: 'root'
})
export class OurNumbersService {

  static readonly API_PATH_PRODUCTS = "/products"
  static readonly API_PATH_PROVIDERS = "/providers"

  constructor(private http: HttpClient) {
  }

  getItems(): Observable<ResultItem[]> {
    return this.http.get<ResultItem[]>(environment.API_URL + OurNumbersService.API_PATH_PRODUCTS)
  }

  //@todo app already has a provider service - discuss issue!
  getProviders(): Observable<Provider[]> {
    return this.http.get<Provider[]>(environment.API_URL + OurNumbersService.API_PATH_PROVIDERS)
  }
}
