import {Component, OnInit} from '@angular/core';
import {OurNumbersService} from "./our-numbers.service";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-our-numbers',
  templateUrl: './our-numbers.component.html',
  styleUrls: ['./our-numbers.component.scss']
})
export class OurNumbersComponent implements OnInit {

  products: string[] = [];
  providers: string[] = [];

  constructor(private ourNumbersService: OurNumbersService) {
  }

  ngOnInit(): void {
    this.ourNumbersService.getItems().pipe(
      map((response: any) => {
        response.map((data: any) => {
          this.products.push(data.provider.providerName)
        })
      })
    ).subscribe(data => {
      console.log(this.products)
    })
    this.ourNumbersService.getProviders().pipe(
      map((response: any) => {
          response.map((data: any) => {
            this.providers.push(data.fullName)
          })
        })
    ).subscribe(data => {
      console.log(this.providers)
    })
  }
}
