import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {

  cities: string[] = [];
  newLocation!: string;

  constructor( private readonly router: Router) {
  }

  ngOnInit(): void {
  }

  getNewLocaton(newLocation: string) {
    this.newLocation = newLocation;
  }

  goToResultsPage() {
    if (this.newLocation === undefined) {
      this.router.navigate(["results"])
    } else {
      this.router.navigate(["results"], {queryParams: {city: this.newLocation}})
    }
  }
}
