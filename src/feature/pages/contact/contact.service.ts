import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ContactFormMessage} from './contact-form.interface';


@Injectable({
  providedIn: 'root'
})
export class ContactService {

  static readonly API_URL = `http://localhost:3000/messages`;

  constructor(private http: HttpClient) {
  }

  sendMessage(message: ContactFormMessage): Observable<ContactFormMessage> {
    return this.http.post<ContactFormMessage>(ContactService.API_URL, message);
  }

  getMessage(): Observable<any> {
    return this.http.get<any>(ContactService.API_URL)
  }}
