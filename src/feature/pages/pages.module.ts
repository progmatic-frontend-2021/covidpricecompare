import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactComponent } from './contact/contact.component';
import { CookiesComponent } from './cookies/cookies.component';
import {PagesRoutingModule} from "./pages-routing.module";
import {MatCardModule} from "@angular/material/card";
import {FormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  declarations: [
    AboutUsComponent,
    ContactComponent,
    CookiesComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    MatCardModule,
    FormsModule,
    MatButtonModule
  ],
  exports: [
    AboutUsComponent
  ]
})
export class PagesModule { }
