import {Component, Input, OnInit} from '@angular/core';
import {Provider} from "../providers.interface";

@Component({
  selector: 'app-providers-list',
  templateUrl: './provider-list.component.html',
  styleUrls: ['./provider-list.component.scss']
})
export class ProviderListComponent implements OnInit {

  @Input() providers: Provider[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }


}

