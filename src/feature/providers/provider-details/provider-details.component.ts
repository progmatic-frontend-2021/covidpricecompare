import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProvidersService} from "../providers.service";
import {Products, Provider} from "../providers.interface";

@Component({
  selector: 'app-provider-details',
  templateUrl: './provider-details.component.html',
  styleUrls: ['./provider-details.component.scss']
})
export class ProviderDetailsComponent implements OnInit {

provider!: Provider;

providerId!: string;
products: Products[] = [];

  constructor(private route: ActivatedRoute, private providerService: ProvidersService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.providerId = params.id;
    })
    this.providerService.getProvidersById(this.providerId).subscribe(provider => {
      this.provider = provider;
    })
    this.providerService.getProductsByProviderId(this.providerId).subscribe(product => {
      this.products = product;
    })
  }
}
