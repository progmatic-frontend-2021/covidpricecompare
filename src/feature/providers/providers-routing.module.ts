import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import {ProvidersContainerComponent} from "./providers-container/providers-container.component";
import {ProviderDetailsComponent} from "./provider-details/provider-details.component";

const routes: Routes = [
  { path: '', component: ProvidersContainerComponent },
  { path: ':id', component: ProviderDetailsComponent },
  { path: ':id/:name', component: ProviderDetailsComponent }
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ProvidersRoutingModule { }


