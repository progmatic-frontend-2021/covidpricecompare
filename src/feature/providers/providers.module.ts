import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProvidersContainerComponent } from './providers-container/providers-container.component';
import { ProvidersRoutingModule} from "./providers-routing.module";
import { ProvidersItemComponent } from './provider-item/providers-item.component';
import { ProviderListComponent } from './providers-list/provider-list.component';
import { ProviderDetailsComponent } from './provider-details/provider-details.component';
import {ProvidersService} from "./providers.service";
import {HttpClientModule} from "@angular/common/http";
import {MatIconModule} from "@angular/material/icon";
import { ProviderSortingComponent } from './provider-sorting/provider-sorting.component';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule} from "@angular/forms";
import {MatCardModule} from "@angular/material/card";
import {MatDividerModule} from "@angular/material/divider";
import {ShareButtonModule} from "ngx-sharebuttons/button";
import {ShareIconsModule} from "ngx-sharebuttons/icons";
import {ShareButtonsModule} from "ngx-sharebuttons/buttons";
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  declarations: [
    ProvidersContainerComponent,
    ProvidersItemComponent,
    ProviderListComponent,
    ProviderDetailsComponent,
    ProviderSortingComponent
  ],
  imports: [
    CommonModule,
    ProvidersRoutingModule,
    MatIconModule,
    HttpClientModule,
    MatSelectModule,
    FormsModule,
    MatCardModule,
    MatDividerModule,
    ShareIconsModule,
    ShareButtonsModule,
    MatButtonModule
  ],
  providers: [ProvidersService]
})

export class ProvidersModule { }
