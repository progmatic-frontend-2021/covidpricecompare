
export interface Provider {
  id: number,
  _id: string,
  shortName: string,
  fullName: string,
  email: string,
  phone: string,
  taxId: number,
  web: string,
  address: Address,
  openHours: OpenHours[],
  paymentInfo: PaymentOptions,
  serviceLocations: Location[],
  providerLogo: string;
}

export interface Address {
  city: string,
  coordinates: [number, number],
  streetName: string,
  streetNumber: string,
  postalCode: number,
  country: string
}

export interface OpenHours {
  weekday: string,
  openInterval: TimeInterval[]
}

export interface TimeInterval {
  fromHour: string | number,
  toHour: string | number
}

export interface PaymentOptions {
  cash: boolean,
  bankCard: boolean,
  online: boolean
}

export interface Location extends Address {
  name: string,
  openHours: OpenHours[]
}

export interface Products {
  _id: string;
  price: number;
  type: string;
  provider: string;
  productLink: string
}
