import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Products, Provider} from "./providers.interface";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ProvidersService {

  static readonly API_PATH_PROVIDERS = "/providers"

  static readonly API_PATH_PRODUCT = "/products/findByProviderId"

  constructor(private http: HttpClient) {
  }

  getProviders(): Observable<Provider[]> {
    return this.http.get<Provider[]>(environment.API_URL + ProvidersService.API_PATH_PROVIDERS)
  }

  getProvidersById(id: string): Observable<Provider> {
    return this.http.get<Provider>(environment.API_URL + ProvidersService.API_PATH_PROVIDERS + '/' + id);
  }

  getProductsByProviderId(provider: string): Observable<Products[]> {
    return this.http.get<Products[]>(environment.API_URL + ProvidersService.API_PATH_PRODUCT + '/' + provider)
  }
}
