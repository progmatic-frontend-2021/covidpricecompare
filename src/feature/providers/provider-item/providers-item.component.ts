import {Component, Input, OnInit} from '@angular/core';
import {Provider} from "../providers.interface";
import {ProvidersService} from "../providers.service";

@Component({
  selector: 'app-provider-item',
  templateUrl: './providers-item.component.html',
  styleUrls: ['./providers-item.component.scss']
})
export class ProvidersItemComponent implements OnInit {

  @Input() provider!: Provider;
  productsLength!: number;

  constructor(private providerService: ProvidersService ) { }

  ngOnInit(): void {
    this.providerService.getProductsByProviderId(this.provider._id).subscribe(res => {
      this.productsLength = res.length
    })
  }

}
