import { Component, OnInit } from '@angular/core';
import {MatOption, MatOptionSelectionChange} from "@angular/material/core";

interface Option {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-provider-sorting',
  templateUrl: './provider-sorting.component.html',
  styleUrls: ['./provider-sorting.component.scss']
})
export class ProviderSortingComponent {

  selectedOption = 'Rendezés ABC sorrendben'
  orderOptions: Option[] = [
    {value: 'order-abc', viewValue: 'Rendezés ABC sorrendben'},
    {value: 'products-desc', viewValue: 'Legtöbb termék elöl'},
    {value: 'products-asc', viewValue: 'Legkevesebb termék elöl'},
    {value: 'rating-desc', viewValue: 'Legjobbra értékelt elöl'},
    {value: 'rating-asc', viewValue: 'Legrosszabbra értékelt elöl'},
  ];

  change(event: any) {
    console.log(event.source.value, event);
  }
}
