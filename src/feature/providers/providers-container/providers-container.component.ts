import {Component, OnInit} from '@angular/core';
import {ProvidersService} from "../providers.service";
import {Provider} from "../providers.interface";

@Component({
  selector: 'app-providers-container',
  templateUrl: './providers-container.component.html',
  styleUrls: ['./providers-container.component.scss']
})
export class ProvidersContainerComponent implements OnInit {

  providers: Provider[] = [];

  constructor(private providerService: ProvidersService) {
  }

  ngOnInit(): void {
    this.providerService.getProviders().subscribe(data => {
      this.providers = data;
    })
  }
}






