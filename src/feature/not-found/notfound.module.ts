import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NotFoundComponent} from "./not-found/not-found.component";
import {MatButtonModule} from "@angular/material/button";
import {LocationModule} from "../../shared/location-selector/location.module";
import {LocationSelectorComponent} from "../../shared/location-selector/location-selector.component";

@NgModule({
  declarations: [
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule
  ],
  exports: [
    NotFoundComponent
  ]
})
export class NotfoundModule { }
