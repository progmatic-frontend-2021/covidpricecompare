import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";
import {LandingComponent} from "../feature/landing/landing.component";
import {NotFoundComponent} from "../feature/not-found/not-found/not-found.component";

const routes: Routes = [
  {
    path: '', component: LandingComponent,
  },
  {
    path: 'results', loadChildren: () => import('../feature/results/results.module').then(m => m.ResultsModule)
  },
  {
    path: 'providers', loadChildren: () => import('../feature/providers/providers.module').then(m => m.ProvidersModule)
  },
  {
    path: '', loadChildren: () => import('../feature/pages/pages.module').then(m => m.PagesModule)
  },
  {
    path: '404', component: NotFoundComponent,
  },
  {
    path: '**', redirectTo: '/404', pathMatch: 'full',
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })
  ],
  exports: [
    RouterModule
  ],
})
export class CoreRoutingModule { }
