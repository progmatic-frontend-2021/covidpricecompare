import { Component, OnInit, HostBinding } from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-core',
  templateUrl: './core.component.html',
  styleUrls: ['./core.component.scss']
})
export class CoreComponent implements OnInit {

  @HostBinding('class') classname = '';
  toggleControl = new FormControl(false);

  constructor() { }

  ngOnInit(): void {
    this.toggleControl.valueChanges.subscribe(val => {
      this.classname = val ? 'darkMode' : '';
    })
  }
}
