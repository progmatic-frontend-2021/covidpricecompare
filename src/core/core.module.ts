import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreComponent } from './core/core.component';
import { CoreRoutingModule } from "./core-routing.module";
import { LandingModule } from "../feature/landing/landing.module";
import { RouterModule } from "@angular/router";
import {SharedModule} from "../shared/shared.module";
import {NotfoundModule} from "../feature/not-found/notfound.module";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {MatIconModule} from "@angular/material/icon";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    CoreComponent
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    LandingModule,
    SharedModule,
    NotfoundModule,
    MatSlideToggleModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    CoreComponent
  ]
})
export class CoreModule { }
