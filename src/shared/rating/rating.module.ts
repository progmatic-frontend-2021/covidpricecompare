import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RatingComponent} from "./rating.component";
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";



@NgModule({
  declarations: [
    RatingComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule
  ],
  exports: [
    RatingComponent
  ]
})
export class RatingModule { }
