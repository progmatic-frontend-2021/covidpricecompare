import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

  ratingNum: number = 200
  rating: number = 4.5

  thumbsUp: number = 219
  thumbsDown: number = 23

  constructor() { }

  ngOnInit(): void {
  }

  /* Like / Dislike option

  clickCountUp(): void{
    this.thumbsUp++
  }

  clickCountDown(): void{
    this.thumbsDown--
  }

   */
}

