import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LocationSelectorComponent} from "./location-selector.component";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    LocationSelectorComponent
  ],
  exports: [
    LocationSelectorComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ]
})
export class LocationModule { }
