import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  static readonly API_PATH_LOCATIONS = "/locations";

  constructor(private http: HttpClient) { }

  getLocations(): Observable<string[]> {
    return this.http
      .get<string[]>(environment.API_URL + LocationService.API_PATH_LOCATIONS)
  }

}
