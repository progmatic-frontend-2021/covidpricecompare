import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {LocationService} from "./location.service";
import {ActivatedRoute} from "@angular/router";
import {FormBuilder} from "@angular/forms";

interface Location {
  location: string
}

@Component({
  selector: 'app-location-selector',
  templateUrl: './location-selector.component.html',
  styleUrls: ['./location-selector.component.scss']
})
export class LocationSelectorComponent implements OnInit {

  cities: string[] = [];
  @Output() newLocation:EventEmitter<string> = new EventEmitter<string>();
  param!: string | null;

  constructor(private locationsService: LocationService, private route: ActivatedRoute, private formBuilder: FormBuilder) {
  }

  locationForm = this.formBuilder.group({
    location: null
  });

  ngOnInit(): void {
    this.locationsService.getLocations()
      .subscribe(data => {
        this.cities = data
      })
    this.locationForm.patchValue({location: null})
    this.route.queryParamMap.subscribe(params => {
      this.param = params.get('city')
    });
    if (this.param) {
      this.locationForm.patchValue({location: this.param})
    } else {
      this.locationForm.setValue({location: null})
    }
  }

  onLocationChange(value: Location) {
    this.newLocation.emit(value.location);
  }
}
