import { Component, OnInit,Input } from '@angular/core';
import {Location} from "../../../feature/providers/providers.interface";

@Component({
  selector: 'app-locations-list',
  templateUrl: './locations-list.component.html',
  styleUrls: ['./locations-list.component.scss']
})
export class LocationsListComponent implements OnInit {

  @Input() locations!: Location[];
  location!: Location;

  constructor() { }

  ngOnInit(): void {
  }

}
