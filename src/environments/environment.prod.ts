export const environment = {
  production: true,
  API_URL: "https://covid360-backend.herokuapp.com/",
  APP_TITLE: 'covid360'
};
