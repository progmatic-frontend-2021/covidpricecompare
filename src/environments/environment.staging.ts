export const environment = {
  production: false,
  API_URL: "https://covid360-backend.herokuapp.com",
  APP_TITLE: 'covid360'
};
